# -*- coding: utf-8 -*-
import os
import re
from gnomevfs import URI

NAME_AND_PATH_SEP = "  -----  "

(FILES_FILE_URI_IDX,
 FILES_FILE_IDX,
 FILES_FILEPATH_IDX) = range(3)
(VISIBILE_STRING,
 FILE) = range(2)

class TmGotoFileFilesHelper(object):
    
    def __init__(self, window_helper, liststore):
        self._window_helper = window_helper
        self._window = self._window_helper.get_window()
        self._root_path = ""
        self._liststore = liststore
        self._files = {}
        self._folders_blacklist = [".git", ".bzr", ".svn"]
        self._filtering_regex = None
        self._never_updated = True
   
    def get_root_path(self):
        return self._root_path
    def set_root_path(self, path):
        self._root_path = path
    root_path = property(get_root_path, set_root_path)
    
    def get_never_updated(self):
        return self._never_updated
    def set_never_updated(self, state):
        self._never_updated = state
    never_updated = property(get_never_updated, set_never_updated)
    
    def populate(self):
        
        """ Collect recursively project's files name and fill in Treeview """
        
        print "[TmGotoFilePlugin] TmGotoFileFilesHelper::populate"
        print "[TmGotoFilePlugin] self.root_path: %s" % self.root_path
        
        if not self.root_path == "":
            
            _, _, project_root_folder = self.root_path.rpartition("/")
            
            # Regex to blacklist some folders like .git
            regex = "(%s)" % "|".join(list(self._folders_blacklist))
            blacklist_regex = re.compile(regex)
            
            for root, subFolders, files in os.walk(self.root_path):
                _, _, current_sub_folder = root.rpartition("/")
                if not re.search(blacklist_regex, root):
                    if not project_root_folder == "":
                        _, _, file_path = root.rpartition(project_root_folder)
                        file_path = file_path[1:] if not file_path == "" else ""
                    else:
                        file_path = ""
                    for file in files:
                        file_uri = URI(os.path.join(root, file))
                        visible_string = file.replace("&", "&amp;")
                        if not file_path == "":
                            visible_string += NAME_AND_PATH_SEP + file_path
                        self._on_new_item(visible_string, [file_uri, file, file_path])
            
            self._on_treatment_finished()
    
    def _on_new_item(self, visible_string, item):
        """
        Called method when a file is available to add in the list
        """
        self._files[visible_string] = item
    
    def _on_treatment_finished(self):
        """
        Called method when treatment have finished
        """
        for visible_string in sorted(self._files.iterkeys()):
            self._liststore.append([visible_string, self._files[visible_string][FILES_FILE_URI_IDX]])
        
        print "[TmGotoFilePlugin] %d rows added!" % self.row_count()
    
    def filter_with(self, query_string):
        
        """ Apply query string on project's files list' """
        
        # Do not use directly the query string from input
        query_string = self._filter_query_string(query_string)
        
        # Prepare Regex
        self._filtering_regex = re.compile("(%s)" % (".*".join(list(query_string))), re.IGNORECASE)
        coloration_regex = re.compile("(%s)" % ("|".join(list(query_string))), re.IGNORECASE)
        
        # Generate a table of the visibile_string (to identify item from self._files dict)
        # and the filename from the self._files dict
        files = []
        for visible_string in sorted(self._files.iterkeys()):
            files.append([visible_string, self._files[visible_string][FILES_FILE_IDX]])
        
        self._liststore.clear()
        
        # Filter generated table using regex and only insert in the self._liststore
        # items that match query string
        for visible_string_and_file in filter(self._filter_on_filename, files):
            # Get back self._files dict instance of matching item
            file = self._files[visible_string_and_file[VISIBILE_STRING]]
            # Regenerate visible_string
            visible_string = file[FILES_FILE_IDX]
            visible_string = re.sub(coloration_regex, r'<b>\1</b>', visible_string)
            visible_string = visible_string.replace("&", "&amp;")
            if not file[FILES_FILEPATH_IDX] == "":
                visible_string += NAME_AND_PATH_SEP + file[FILES_FILEPATH_IDX]
            # Populate the gtk.Listore
            self._liststore.append([visible_string, file[FILES_FILE_URI_IDX]])
    
    def _filter_on_filename(self, visible_string_and_file):
        return True if not self._filtering_regex.search(visible_string_and_file[FILE]) is None else False
    
    def _filter_query_string(self, query_string):
        
        """ Filter user input to avoid regex failure """
        
        query_string = re.sub(r'[^a-zA-Z0-9_\&]', '', query_string)
        query_string = query_string.replace("*", "")
        return query_string
    
    def row_count(self):
        return len(self._liststore)
    
    def is_in_project_path(self, path):
        return path.startswith(self.root_path)
