import gtk

class TmGotoFileMenuHelper(object):
    
    def __init__(self, window_helper):
        self._window_helper = window_helper
        self._window = self._window_helper.get_window()
        self._action_group = gtk.ActionGroup("TmGotoFileMenuAction")
        self._ui_id = None
   
    def insert(self):
        print "[TmGotoFilePlugin] TmGotoFileMenuHelper::insert"
        action = ("Go to File",
            None,
            "Go to File",
            "<Ctrl>T",
            "Go to File",
            lambda x, y: self._window_helper.show_gotofile_window(y)
        )
        
        # Create a new action group
        self._action_group.add_actions([action], self._window)
        
        manager = self._window.get_ui_manager()
        manager.insert_action_group(self._action_group, -1)
        self._ui_id = manager.new_merge_id()
        manager.add_ui(self._ui_id,
            "/MenuBar/FileMenu/FileOps_2",
            "Go to File",
            "Go to File",
            gtk.UI_MANAGER_MENUITEM,
            True
        )
    
    def remove(self):
        print "[TmGotoFilePlugin] TmGotoFileMenuHelper::remove"
        if not self._ui_id is None:
            manager = self._window.get_ui_manager()
            manager.remove_ui(self._ui_id)
            manager.remove_action_group(self._action_group)
            self._action_group = None
            manager.ensure_update()
