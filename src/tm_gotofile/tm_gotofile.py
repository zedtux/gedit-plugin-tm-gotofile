import os
import gtk
import gedit

from tm_gotofile_menu_helper import TmGotoFileMenuHelper
from tm_gotofile_files_helper import TmGotoFileFilesHelper
from gnomevfs import URI

class TmGotoFileWindowHelper(object):
    
    """ 
    Window Helper
    Create the menu and Gtk accelerator
    Show the gtk.Window when accelerator <Alt>T used.
    Fill in gtk.Treeview with current project content and filter
    on filename when typing into the gtk.Entry entrySearch
    """
    
    def __init__(self, window):
        self._window = window
        self._project_path = ""
        self._plugin_menu = TmGotoFileMenuHelper(self)
        
        # Iniialize gtk window
        self.builder = gtk.Builder()
        self.builder.add_from_file(os.path.join(os.path.dirname(__file__), "gotofile.glade"))
        self.builder.connect_signals(self)
        
        self._window_goto_file = self.builder.get_object("windowGotoFile")
        self._window_goto_file.set_transient_for(self._window)
        self._window_goto_file.hide()
        self._entry_search = self.builder.get_object("entrySearch")
        self._treeview_results = self.builder.get_object("treeviewResults")
        self._liststore_results = self.builder.get_object("liststoreResults")
        
        self._project_files = TmGotoFileFilesHelper(self, self._liststore_results)
    
    def get_window(self):
        return self._window
    
    def get_project_path(self):
        return self._project_path
    def set_project_path(self, path):
        self._project_path = path
    project_path = property(get_project_path, set_project_path)
    
    def get_project_files(self):
        return self._project_files
    def set_project_files(self, instance):
        self._project_files = instance
    project_files = property(get_project_files, set_project_files)
    
    def deactivate(self):
        self._window = None
        self._plugin_menu.remove()
        self._plugin_menu = None
        self.project_files = None
    
    def update_project_files_list(self):
        if self.project_files.never_updated:
            self.project_files.root_path = self.project_path
            self.project_files.populate()
            self._select_first_row()
            
            if self.project_files.row_count() > 0:
                self.project_files.never_updated = False
                self._plugin_menu.insert()
            else:
                print "[TmGotoFilePlugin] %d row%s is not enough to insert the menu!" % \
                (self.project_files.row_count(), "s" if self.project_files.row_count() > 0 else "")
    
    def _open_file(self, uri):
        already_open = False
        for doc in self._window.get_documents():
            if doc.get_uri() == uri:
                self._window.set_active_tab(gedit.tab_get_from_document(doc))
                already_open = True
                break
        if not already_open:
            self._window.create_tab_from_uri(uri, None, 0, False, True)
    
    def _select_first_row(self):
        if self.project_files.row_count() > 0:
            iter = self._liststore_results.get_iter_first()
            if iter != None:
                selection = self._treeview_results.get_selection()
                if selection:
                    selection.select_iter(iter)
    
    def _hide_and_reset(self):
        self._window_goto_file.hide()
        self._entry_search.set_text("")
        self._select_first_row()
    
    #---------------------------------------------------------------------------
    # Gtk Callback methods
    #---------------------------------------------------------------------------
    def show_gotofile_window(self, window):
        """ Callback method when <Alt>T accelerator used """
        self._window_goto_file.show()
        self._entry_search.grab_focus()
    
    def on_entrySearch_icon_press(self, widget, icon, event):
        if icon == gtk.ENTRY_ICON_SECONDARY:
            widget.set_text("")
    
    def on_entrySearch_key_release_event(self, widget, event):
        if event.keyval == gtk.keysyms.Return:
            self.on_treeviewResults_row_activated(None, None, None)
            return
    
    def on_treeviewResults_row_activated(self, widget, path, view_column):
        model, iter = self._treeview_results.get_selection().get_selected()
        if iter:
            self._open_file(model.get_value(iter, 1))
            self._hide_and_reset()
    
    def on_entrySearch_changed(self, editable):
        self.project_files.filter_with(editable.get_text())
        self._select_first_row()
    
    def on_windowGotoFile_key_release_event(self, widget, event):
        if event.keyval == gtk.keysyms.Escape:
            self._hide_and_reset()

class TmGotoFilePlugin(gedit.Plugin):
    
    """ Gedit Plugin Interface """
    
    def __init__(self):
        gedit.Plugin.__init__(self)
        self._instances = {}
        self._signal_id = None
    
    def activate(self, window):
        # Prepare the 'Go to File' window
        self._instances[window] = TmGotoFileWindowHelper(window)
        
        # This signal will be used for 2 things:
        #   - Close a tab if path is not a file
        #   - Update 'Go to File' window content when browsing HDD with
        #     FileBrowser plugin.
        self._signal_id = window.connect("tab-added", self.on_tab_added)
    
    def on_tab_added(self, window, tab):
        """ Small behavior to allow the `gedit .` to work like `mate .` """
        doc_path = tab.get_document().get_uri_for_display()
        print "[TmGotoFilePlugin] doc_path: %s" % doc_path
        
        doc_path_dirname = ""
        if not os.path.exists(doc_path):
            pass
        elif not os.path.isfile(doc_path):
            # If current tab point to a folder ('gedit .')
            # close it, and use it as project's root path for the plugin.
            window.close_tab(tab)
            doc_path_dirname = doc_path
        else:
            # If current tab point to a file
            doc_path_dirname = os.path.dirname(doc_path)
            print "[TmGotoFilePlugin] doc_path's dirname: %s" % doc_path_dirname
            
            # if 'Go to File' wasn't updated before:
            # -> Use the file's dirname as project's source
            if self._instances[window].project_files.never_updated:
                print "[TmGotoFilePlugin] Use doc_path as project's path!"
                
            # if 'Go to File' was updated but the doc_path is not in the
            # registered project's path:
            # -> Open a new gedit window with the document like TextMate do.
            elif not self._instances[window].project_files.is_in_project_path(doc_path):
                print "[TmGotoFilePlugin] Open this document in a new gedit instance!"
                new_gedit_instance = gedit.app_get_default().create_window()
                window.close_tab(tab)
                print "[TmGotoFilePlugin] Opening tab for %s" % doc_path
                new_gedit_instance.create_tab_from_uri(str(URI(doc_path)), None, 0, False, True)
            # Otherwise, if 'Go to File' was updated
            # and the doc_path is in the project's path:
            # -> Don't do anything
            else:
                print "[TmGotoFilePlugin] Don't do anything!"
        
        if not doc_path_dirname == "":
            print "[TmGotoFilePlugin] Calling update_gotofile_window_from(window, '%s')" % doc_path
            self.update_gotofile_window_from(window, doc_path_dirname)
    
    def update_gotofile_window_from(self, window, path):
        self._instances[window].project_path = path
        self._instances[window].update_project_files_list()
    
    def deactivate(self, window):
        window.disconnect(self._signal_id)
        self._instances[window].deactivate()
        del self._instances[window]
