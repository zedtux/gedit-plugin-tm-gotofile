# -*- coding: utf-8 -*-
from distutils.core import setup

setup(name='gedit-tm-gotofile',
      version='1.0',
      data_files=[
        ("/usr/lib/gedit-2/plugins/", ["src/tm_gotofile.gedit-plugin"]),
        ("/usr/lib/gedit-2/plugins/tm_gotofile/", ["src/tm_gotofile/gotofile.glade",
                                                   "src/tm_gotofile/__init__.py",
                                                   "src/tm_gotofile/tm_gotofile.py",
                                                   "src/tm_gotofile/tm_gotofile_files_helper.py",
                                                   "src/tm_gotofile/tm_gotofile_menu_helper.py"])
      ],
      author='Guillaume Hain',
      author_email='zedtux@zedroot.org',
      maintainer='zedtux',
      maintainer_email='zedtux@zedroot.org',
      url='https://github.com/zedtux/gedit-tm-gotofile',
      keywords=['gedit','plugins', 'gotofile', 'textmate'],
      license='GPLv3',
      description='TextMate like \'Go to File\' for Gedit.'
)
